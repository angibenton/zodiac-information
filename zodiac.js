/**
 * STARSIGNS
 * 0  - Capricorn   dec 22 - jan 19
 * 1  - Aquarius    jan 20 - feb 18
 * 2  - Pisces      feb 19 - mar 20
 * 3  - Aries       mar 21 - apr 19
 * 4  - Taurus      apr 20 - may 20
 * 5  - Gemini      may 21 - june 20
 * 6  - Cancer      jun 21 - jul 22
 * 7  - Leo         jul 23 - aug 22
 * 8  - Virgo       aug 23 - sep 21
 * 9  - Libra       sep 22 - oct 23
 * 10 - Scorpio     oct 24 - nov 21
 * 11 - Sagittarius nov 22 - dec 21
 */

/**
 * CHINESE ZODIAC BY YEAR % 12
 * 0  - Monkey
 * 1  - Rooster
 * 2  - Dog
 * 3  - Pig
 * 4  - Rat
 * 5  - Ox
 * 6  - Tiger
 * 7  - Rabbit
 * 8  - Dragon
 * 9  - Snake
 * 10 - Horse
 * 11 - Ram
 */


document.addEventListener("DOMContentLoaded", init);

/**
 * Adds the event listener once the DOM Content is loaded
 */
function init(){
    document.querySelector("#ZodiacButton").addEventListener("click", processInput);
}

/**
 * Runs the zodiac page (Event handler for clicking the button)
 */
function processInput(e){
    let bday = getBday();
    let star = starSign(bday);
    let chinese = chineseZodiac(bday);
    showZodiac(star, chinese);
}


/**
 *
 * @returns {Date} bday The birthday collected from the HTML date form
 */
function getBday() {
    let bday = document.getElementById("bday").value;
    let bdaySlashFormat = bday.replace(/-/, '/').replace(/-/, '/');
    //the Date constructor pushes the day back by 1, but not if the format is switched from 2020-01-23 to 2020/01/23
    return new Date(bdaySlashFormat);
}

/**
 *
 * @param {Date}    bday  The date to be translated to a star sign
 * @return {Number} star  An integer between 0 and 11, which encodes the star sign where 0 = Capricorn
 */
function starSign(bday) {
    //NON LEAP
    let daysUpToMonth = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334]; //total days before the first of each month
    let starEndDay = [19, 31+18, 59+20, 90+19, 120+20, 151+20, 181+22, 212+22, 243+21, 273+23, 304+21, 334+21];//last day of the starsign
    //e.g. last day of capricorn is jan 19.

    let month = bday.getMonth();
    let days = daysUpToMonth[month] + bday.getDate();

    if(isLeap(bday.getFullYear())){
        if(month > 1) { days++; }
        //fix the starsign end days, starting with
        for(let i = 2; i < 12; i++){ starEndDay[i]++; }
    }

    let star = 0;
    //loop through the star signs in chronological order
    for(let i = 0; i < 12; i++) {
        //stop at the first sign for which days is less than the last day
        if(days <= starEndDay[i]) {
            star = i;
            break;
        }
    }
    return star;
}


/**
 *
 * @param {Date}    bday    The Birthday to be translated to a Chinese zodiac
 * @return {Number} chinese An integer between 0 and 11, which encodes the Chinese zodiac where 0 = Rat
 */
function chineseZodiac(bday) {
    let yr = bday.getFullYear();
    return yr % 12;
}

/**
 *
 * @param {Number}   year The input year
 * @return {Boolean} leap Whether or not this is a leap year
 */
function isLeap(year){
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}


/**
 *
 * @param {Number} star    An integer between 0 and 11, which encodes the star sign where 0 = Capricorn
 * @param {Number} chinese An integer between 0 and 11, which encodes the chinese zodiac where 0 = Rat
 */
function showZodiac(star, chinese) {
    clearZodiac();
    document.getElementById("s" + star).style.display = "block";
    document.getElementById("c" + chinese).style.display = "block";
    document.getElementById("sources").style.display = "block";
}

/**
 * Clears the displayed zodiac signs, to be used before displaying a new person's zodiac
 */
function clearZodiac(){
    let star_elements = document.querySelectorAll("div.star");
    let chinese_elements = document.querySelectorAll("div.chinese");
    for(let i = 0; i < 12; i++){
        star_elements[i].style = "none";
        chinese_elements[i].style = "none";
    }
}

